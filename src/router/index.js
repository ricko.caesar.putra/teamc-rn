import React, { Component } from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from "@react-navigation/stack";
import { LoginPage, SplashScreen, RegisterPage, HomePage, ProfilePage, UserReviewPage } from '../pages';
import { colors } from '../utils';
import Icon from 'react-native-vector-icons/Ionicons'
import { View } from 'react-native';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs'

const MaterialBottom = createMaterialBottomTabNavigator();
const Stack = createStackNavigator();

const HomeStack = () => {
    return (
        <Stack.Navigator initialRouteName="Home">
            <Stack.Screen
                name="SplashScreen"
                component={SplashScreen}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="LoginPage"
                component={LoginPage}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="RegisterPage"
                component={RegisterPage}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="HomePage"
                component={HomePage}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="ProfilePage"
                component={ProfilePage}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="Home"
                component={BotomTab}
                options={{
                    headerShown: false,
                }}
            />
        </Stack.Navigator>
    )
}


const BotomTab = () => {
    return (
        <MaterialBottom.Navigator
            shifting={false}
            // initialRouteName="Home"
            barStyle={{
                backgroundColor: colors.temaCard,
                borderWidth: .5,
                borderColor: colors.temaCard,
                height:60,
                justifyContent:'center',
                paddingTop:10
                
            }}
            activeColor={colors.temaCard}
        inactiveColor='black'
        >
            <MaterialBottom.Screen name="UserReviewPage" component={UserReviewPage}
                options={{
                    tabBarLabel:'',
                    tabBarIcon: ({ color }) => (
                        <View style={{ marginTop: -2, width: 30, height: 30, borderRadius: 15, alignItems: 'center', justifyContent: 'center' }}>
                            <Icon name={color == colors.temaCard ? 'ios-chatbox' : 'ios-chatbox-outline'} size={25} color='black' />
                            {/* <Icon name='ios-person' size={20} color='white' /> */}
                        </View>
                    )
                }}
            />
            <MaterialBottom.Screen name="HomePage" component={HomePage}
                options={{
                    tabBarLabel: '',
                    tabBarIcon: ({ color }) => (
                        <View style={{ marginTop: -2, width: 30, height: 30, borderRadius: 15, alignItems: 'center', justifyContent: 'center' }}>
                             <Icon name={color == colors.temaCard ? 'ios-home' : 'ios-home-outline'} size={25} color='black' />
                            {/* <Icon name='ios-person' size={20} color='white' /> */}
                        </View>
                    )
                }}
            />

            <MaterialBottom.Screen name="ProfilePage" component={ProfilePage}
                options={{
                    tabBarLabel: '',
                    tabBarIcon: ({ color }) => (
                        <View style={{ marginTop: -2, width: 30, height: 30, borderRadius: 15, alignItems: 'center', justifyContent: 'center' }}>
                            <Icon name={color == colors.temaCard ? 'ios-person' : 'ios-person-outline'} size={25} color='black' />
                            {/* <Icon name='ios-person' size={20} color='white' /> */}
                        </View>
                    )
                }}
            />


        </MaterialBottom.Navigator>
    )

}


export default class Router extends Component {
    render() {
        return (
            <NavigationContainer>
                <HomeStack />
            </NavigationContainer>
        )
    }
}